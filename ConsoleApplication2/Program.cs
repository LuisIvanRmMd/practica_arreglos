﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        /// 1.-  Que rellene un array con los 100 primeros números enteros y los muestre en pantalla en orden ascendente.
        static void Main(string[] args)
        {
            int numero1;
            Console.WriteLine("El tamaño del arreglo es de:");
            numero1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Los números que contiene, en orden ascendente, son:");
            for (int i = 1; i <= numero1; i++)
            {
                int[] arreglo = new int[i];
                Console.WriteLine(i);
            }
            Console.ReadKey();

            /// 2.- Que rellene un array con los 100 primeros números enteros y los muestre en pantalla en orden descendente. 
            Console.WriteLine("Los números que contiene, en orden descendente, son:");
            for (int i = numero1; i > 0; i--)
            {
                int[] arreglo = new int[i];
                Console.WriteLine(i);
            }
            Console.ReadKey();

            /// 3.- Que lea 10 números por teclado, los almacene en un array y muestre la suma, resta, multiplicación y división de todos.
            
        }
    }
}
